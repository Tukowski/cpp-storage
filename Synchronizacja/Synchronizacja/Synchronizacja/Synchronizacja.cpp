﻿// Synchronizacja.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include "pch.h"
#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

mutex myMutex;

void shared_cout(int i)
{
	lock_guard<mutex> g(myMutex);
	cout << " " << i << " ";
}

void f(int n)
{
	for (int i = 10 * (n - 1); i < 10 * n; i++) {
		shared_cout(i);
	}
}

int main()
{
	thread t1(f, 1);  // 0-9
	thread t2(f, 2);  // 10-19

	for (int i = 0; i > -50; i--)
		shared_cout(i);  // (0, -49)

	t1.join();
	t2.join();
	
	return 0;
}