﻿#include "pch.h"
#include <iostream>
#include <vector>


class A {
public:
	char* poleChar;

	A(const std::string& stalaReferencjaTekst) {
		poleChar = new char[stalaReferencjaTekst.length() + 1];  // plus jeden bo kończący NULL. Pomiędzy znakiem pierwszym a ostatnim.
		memcpy(poleChar, stalaReferencjaTekst.c_str(),(stalaReferencjaTekst.length() + 1));
		std::cout << "standard constructor\n";
	}
	A(const A& obiekt) { //Lvalue
		size_t size = strlen(obiekt.poleChar) + 1;
		poleChar = new char[size];
		memcpy(poleChar, obiekt.poleChar, size);
		std::cout << "copy constructor\n";
	}
	A(A&& obiekt) { //referencja do Rvalue bo &&
		poleChar = obiekt.poleChar;
		//obiekt.poleChar = nullptr;
		std::cout << "move constructor\n";
	}
	const char* get() const {
		return poleChar;
	}
	
	char* get()	{
		return poleChar;
	}
	
	 ~A(){
	 	delete poleChar;
		std::cout << "Destrutor" << std::endl;
	 }
};

A copy(A const & Lvalue){
	return A(Lvalue);
}
A move(A & Rvalue){
	return A(std::move(Rvalue));
}


int main(){

	const std::string ble = "aaaaa";
	A a("aaa");
	std::cout << a.get() << std::endl;
	A b(copy(a));
	std::cout << b.get() << std::endl;
	//A c = copy(a);
	A c(std::move(b));
	std::cout << c.get() << std::endl;
	//std::cout << b.get() << std::endl;

}