#include "pch.h"
#include <iostream>
#include <vector>
#include <math.h>
#include  <string>
#include <array>

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
1. Napisz funkcję add1 dodającą dwa obiekty dowolnego typu.

Działanie funkcji przetestuj z argumentami tego samego typu numerycznego,
argumentami o różnych typach numerycznych i z dwoma obiektami typu std::string.
*/

template <typename firstType, typename secondType >
auto dodaj(firstType a, secondType b) -> decltype(a + b) {
	return a + b;
}

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Napisz funkcję add2 dodającą dwa obiekty dowolnego typu za pomocą operacji podanej jako argument szablonu.
[](args) -> typezwracany{cialo}(wywoalnie args)
Działanie przetestuj dla typów numerycznych i obiektów std::string oraz operacji zdeﬁniowanej za pomocą lambdy


.
*/

#pragma region addINT, addDOUBLE,subtractINT,subtractDOUBLE

int addINT(int q, int w) {
	return q + w;
}
double addDOUBLE(double q, double w) {
	return q + w;
}
int subtractINT(int q, int w) {
	return q - w;
}
double subtractDOUBLE(double q, double w) {
	return q - w;
}

#pragma endregion

template <typename firstType, typename secondType, typename function>
auto dodaj2(firstType a, secondType b, function operation) -> decltype(operation(a, b)) {
	return operation(a,b);
}

/*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
3. Napisz klasę Wektor z typem danych i rozmiarem podanymi jako dwa parametry szablonu.
 Klasa musi mieć zdeﬁniowany typ value_type i operatory do moyﬁdkacji i pobrania poszczególnych składowych wektora.

W wersji podstawowej konstruktor ma inicjalizować wektor zerami.

W wersji rozszerzonej konstruktor ma inicjować wektor wartościami zdeﬁniowanymi jako trzeci argument template'a.
*/
//rozszerzona
///*
template <typename Type, int Size, Type Value> //<-type Value???
class Wektor {

private: Type tab[Size];

public:
	Wektor() {
		for(int i =0; i<Size;i++) {
			tab[i] = Value;
		}
	}

	using value_type = Type;


	Type & operator[](int whatElement) {
		return tab[whatElement];
	}

	const Type & operator[](int whatElement)const{
		return tab[whatElement];
	}
	//*/

};

/* 

 4. Napisz operator iloczynu skalarnego,
który będzie mógł być użyty do pomnożenia obiektów klas Wektor z zadania 3
i std::vector o rozmiarze 3.

Wersja podstawowa ma działać dla wektorów o takim samym typie elementów.

Wersja rozszerzona ma działać dla wektorów dowolnych typów
	dla których jest zdeﬁniowany operator*, którego wyniki można dodawać operatorem+.

 */
template<class Wektor, class Wektor2>
auto operator * (const Wektor& first, const Wektor2& second) -> decltype(first[0] * second[0])  {
	auto suma = first[0] * second[0] + first[1] * second[1] + first[2] * second[2];

	return suma ;
}




int main() {

	//zad1
	std::cout << "Zadanie1 ---------" << std::endl;
	std::cout << "         --------- 3.9+4" << std::endl;
	std::cout << dodaj(3.9,4) << std::endl;

	//zad2
	std::cout << "Zadanie2 --------- " << std::endl;
	std::cout <<  "        --------- 4.2-3.5" << std::endl;
	std::cout << dodaj2(4.2, 3.5, subtractDOUBLE) << std::endl;
	std::cout << "         --------- Lambda na intach dodawanie 4, 3.5" << std::endl;
	std::cout << dodaj2(4, 3.5, [](int x, double y) -> auto {return x + y; }) << std::endl;
	std::cout << dodaj2(4, 3, [](int x, double y) -> double {return x + y + 0.5; }) << std::endl;

	//zad3
	//rozszerzona
	
	Wektor<int, 2, 0> vectorConst;
	Wektor<int, 2, 0> vector;
	vector[1] = 6;
	Wektor<int, 3, 10>::value_type a;


	std::cout << "Zadanie3 ---------" << std::endl;
	std::cout << "         --------- zczytanie z 2 miejsca wektora zer" << std::endl;
	std::cout << vectorConst[1] << std::endl;
	std::cout << "         --------- zmiana na wektorze" << std::endl;
	std::cout << vector[1] << std::endl;
	//*/
	Wektor<int, 3, 2> vector1;	  
	Wektor<int, 3, 1> vector2;

	std::vector<int> vector3 = { 3, 3, 3};
	
	std::cout << "Zadanie4 ---------" << std::endl;
	std::cout << "Mnozenie --------- Powinno byc 6" << std::endl;
	std::cout << vector1 * vector2 << std::endl;

	return 1;
}
