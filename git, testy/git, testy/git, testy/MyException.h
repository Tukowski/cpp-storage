﻿#pragma once
#include "MyException.h"
#include <stdexcept>
#include <string>
#include <cstring>

class MyException : public std::exception {

	int value;
	std::string communicate;

public:
	MyException(int Value) :
		value(Value)
	{
		communicate = "0 zeros places found and u wanna take: " + std::to_string(value);
	}

	
	virtual const char* what() const throw(){

		return communicate.c_str();
		
	}
};
