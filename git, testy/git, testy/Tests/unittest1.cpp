#include "stdafx.h"
#include "CppUnitTest.h"
#include "../git, testy/Equation.h"
#include "../git, testy/pch.h"
#include <iostream>
#include <assert.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests{	
	
	TEST_CLASS(UnitTest1)
	{
	public:
		/*
		 * Nale�y przetestowa� przypadki braku miejsc zerowych
		 *  oraz funkcje z jednym i dwoma miejscami zerowymi
		 */
		TEST_METHOD(TestMethod1){
			Equation ObjectEquationTest(1, 6, 9);
			Assert::AreEqual(-3, ObjectEquationTest.ZeroPlaces(1), 0.00001);
		}
		TEST_METHOD(TestMethod2){
			Equation ObjectEquationTest(1, 2, 3);
			Assert::ExpectException<MyException>([&]()->double {
				return  ObjectEquationTest.ZeroPlaces(1);
				//nie jako argument. lambda moze dostac dostep do zmiennych ktore ja otaczaja
			});
		}
		TEST_METHOD(TestMethod3){
			Equation ObjectEquationTest(1, -2, -5);
			Assert::AreEqual(-1.44949, ObjectEquationTest.ZeroPlaces(1), 0.00001);
			//Assert::AreSame(-1.44949, ObjectEquationTest.ZeroPlaces(1));
		}
	};
}