﻿#pragma once
#include <iostream>
#include "MyException.h"
#include <iostream>
#include <math.h>
#include <stdexcept>
#include "../git, testy/MyException.h"
#include <string>

class Equation {
public:
	double a, b, c;
	Equation(double A, double B, double C) {
		a = A; b = B; c = C;
	}

public:
	double ZeroPlaces(const int whatZeroPlace_1forFirst_2forSecond) {
		//zminana pierwsza
		//zmiana druga


		const double delta = ((b * b) - (4 * (a*c)));
		
		if (delta > 0) {
			if (whatZeroPlace_1forFirst_2forSecond == 1) {
				return (-b - sqrt(delta)) / (2 * a);
			}
			if (whatZeroPlace_1forFirst_2forSecond == 2) {
				return (-b + sqrt(delta)) / (2 * a);
			}
			else {
				throw std::invalid_argument("Brak " + std::to_string(whatZeroPlace_1forFirst_2forSecond) + " miejsca zerowego");
			}
		}

		if (delta == 0) {
			return -b / (2 * a);
		}
		if (delta < 0) {
			
			throw MyException (whatZeroPlace_1forFirst_2forSecond);
		}
	}
};
