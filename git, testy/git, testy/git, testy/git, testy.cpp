#include "pch.h"
#include "Equation.h"
#include <iostream>
#include <math.h>
#include <stdexcept>
#include <nmmintrin.h>
#include "../git, testy/Equation.h"

/*
 * 
 1. Napisz klasę reprezentującą równanie kwadratowe: f(x) = ax2 + bx + c.
Jej konstruktor powinien przyjmować wartości trzech współczynników a, b i c
typu double.
Dopisz do klasy metodę zwracającą miejsca zerowe równania. Metoda ta powinna
przyjmować jeden argument typu unsigned int mówiący, które miejsce zerowe ma
zwrócić: pierwsze (mniejsze) czy drugie (większe). W przypadku braku żądanego
miejsca zerowego rzucać wyjątkiem.

2.  Napisz klasę wyjątka do zgłaszania braku miejsc zerowych. Powinna ona dziedziczyć
po std::exception i w komunikacie informować o żądanym numerze miejsca
zerowego.

3. Napisz zestaw testów sprawdzających poprawne działanie klasy z pierwszego
zadania. Należy przetestować przypadki braku miejsc zerowych oraz funkcje z jednym
i dwoma miejscami zerowymi.
 */

int main(){

    try {

		//Equation test(1, -2, -5); //2 zeros places
		//Equation test(1, 2, 3); //0 miejsc zerowych
		Equation test(1, 6, 9); //1 miejsce zerowe
		//std::cout << test.ZeroPlaces(1) << std::endl;
		std::cout << test.ZeroPlaces(3) << std::endl;
	
    }
	catch (const std::invalid_argument& deltaBiggerThan0ButBadArgumentFromUser) {
		std::cout << deltaBiggerThan0ButBadArgumentFromUser.what();
	}
	catch (MyException &myexception) {
		std::cout << myexception.what();
	}
}
