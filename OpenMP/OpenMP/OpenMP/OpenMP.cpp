﻿#include "pch.h"
#include <omp.h>
#include <iostream>
#include <ctime>

int main()
{
// #pragma omp parallel 
// #pragma omp critical
// 	std::cout << "Witaj z watku nr " << omp_get_thread_num()
// 		<< " sposrod " << omp_get_num_threads() << "\n";
	const int N = 1000000000;
	int i = 0;
	double sum = 0;
	clock_t t0 = clock();
	
#pragma omp parallel private(i) reduction(+:sum)
	for (i = N - 1; i >= 0; --i)
	{
		sum += 1.0f / (i + 1.0f);
	}

	clock_t t1 = clock();
	printf("s = %g\n", sum);
	printf("t = %g\n", ((double)t1 - t0) / CLOCKS_PER_SEC);


}

/*
 *	Master
 *	Wątki inne niż główny wątek nie wykonają bloku instrukcji powiązanego z tą konstrukcją.
 *	
 *	Single
 *	Dyrektywa pojedyncza omp określa sekcję kodu, która musi być uruchamiana przez pojedynczy dostępny wątek.
 *	
 *	Critical
 *	Dyrektywa krytyczna omp określa sekcję kodu, która musi być wykonywana przez pojedynczy wątek na raz.
 */

/* ZADANIE 3
 * 
 * 
 */
