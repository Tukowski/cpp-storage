﻿#include "pch.h"
#include <iostream>
#include <thread>
#include <future>

void iloczyn_skalarny(std::vector<double> const& v1, std::vector<double> const& v2, std::promise<double> prom) {

	double wynik = 0;
	for (int i = 0; i < v1.size(); i++) {
		wynik += v1[i] * v2[i];
	}
	prom.set_value(wynik);
}

int main() {

	std::vector<double> vFirst = { 2,2 };
	std::vector<double> vSecond = { 3,3 };

	std::thread tabThread[10];
	std::future<double> tabFuture[10];

	double wynik = 0;

	for (int i = 0; i < 10; i++) {
		std::promise<double> promise;
		tabFuture[i] = promise.get_future();
		tabThread[i] = std::thread(iloczyn_skalarny, std::ref(vFirst), std::ref(vSecond), std::move(promise)); //std::move is used to indicate that an object t may be "moved from", i.e. allowing the efficient transfer of resources from t to another object.
	}

	for (int i = 0; i < 10; i++) {
		tabThread[i].join();
		wynik += tabFuture[i].get();
		std::cout << wynik << std::endl;
	}
}