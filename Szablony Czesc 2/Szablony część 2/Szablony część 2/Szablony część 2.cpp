#include "pch.h"
#include <iostream>
#include <string>
#include <math.h>
#include <complex>

using namespace std;

/*
 * Napisz specjalizację szablonu funkcji add1 z poprzedniej listy dodającą dwie dowolne liczby przekazane przez wskaźniki
 */
template <typename firstType, typename secondtype>
auto add(firstType *firstValue, secondtype *secondValue) {
	return *firstValue + *secondValue;
}

/*
 *  Napisz wersję funkcji add1, która będzie łączyć ze sobą dwa teksty przekazane jako const char*
 */
string add(const char *firstValue, const char *secondValue) {
	string temp1 = firstValue;
	temp1 += secondValue;

	return temp1;
}

/*
 *  Napisz szablon, który będzie mógł być użyty do obliczenia w czasie kompilacji objętości hipersześcianu o dowolnej długości boku i ilości wymiarów.
 *  v= a ^n  a bok   n wymiar
 */

template<unsigned long long typeSide, unsigned long long typeDimension>
struct Calculate {
	enum {
		valueVolume = typeSide * Calculate<typeSide, typeDimension - 1>::valueVolume
	};
};

template<unsigned long long typeSide>
struct Calculate<typeSide, 0> {
	enum { valueVolume = 1 };
};

int main() {


	int tempINT = 1;
	double tempDOUBLE = 3.1;

	int *wskTempINT = &tempINT;
	double *wskTempDOUBLE = &tempDOUBLE;


	cout << "Zadanie 1\n" << add(wskTempINT, wskTempDOUBLE) << endl;

	const char *firstTempCHAR = "Raz";
	const char *secondTempCHAR = "Dwa";

	cout << "Zadanie 2\n" << add(firstTempCHAR, secondTempCHAR) << "\n";
	cout << "Zadanie 3\n " << Calculate<5, 4>::valueVolume << endl;

}